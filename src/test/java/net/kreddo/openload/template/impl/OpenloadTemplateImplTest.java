package net.kreddo.openload.template.impl;

import java.net.URI;
import javax.ws.rs.core.UriBuilder;
import net.kreddo.openload.template.OpenloadTemplate;
import org.junit.Before;
import org.junit.Test;

public class OpenloadTemplateImplTest {

  private URI uri;
  private final String key = "tLDzhX-v";
  private final String login = "1caf571cee706a18";
  private final String server = "https://api.openload.co/1";

  private OpenloadTemplate openloadTemplate;

  @Before
  public void setUp() {
    openloadTemplate = new OpenloadTemplateImpl();
  }

  @Test
  public void getAccountInfo() {
    uri = createURI("/account/info");
    System.out.println(openloadTemplate.getAccountInfo(uri));
  }

  @Test
  public void downloadTicket() {
    uri = createURI("/file/dlticket", "pPusXcxiESs");
    System.out.println(openloadTemplate.downloadTicket(uri));
  }

  private URI createURI(String path) {
    return UriBuilder.fromUri(server + path)
        .queryParam("login", login)
        .queryParam("key", key)
        .build();
  }

  private URI createURI(String path, String fileId) {
    return UriBuilder.fromUri(server + path)
        .queryParam("file", fileId)
        .queryParam("login", login)
        .queryParam("key", key)
        .build();
  }
}
