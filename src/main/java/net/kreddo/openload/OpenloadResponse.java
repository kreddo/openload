package net.kreddo.openload;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;
import net.kreddo.openload.dto.Account;

public class OpenloadResponse<T> {

  @JsonProperty("status")
  private int status;
  @JsonProperty("msg")
  private String message;
  @JsonProperty("result")
  private T result;

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public T getResult() {
    return result;
  }

  public void setResult(T result) {
    this.result = result;
  }

  @Override
  public String toString() {
    return "Response{" +
        "status=" + status +
        ", message='" + message + '\'' +
        ", result=" + result +
        '}';
  }
}
