package net.kreddo.openload.template.impl;

import java.net.URI;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import net.kreddo.openload.OpenloadResponse;
import net.kreddo.openload.template.OpenloadTemplate;
import net.kreddo.openload.dto.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OpenloadTemplateImpl implements OpenloadTemplate {

  private static final Logger log = LoggerFactory.getLogger(OpenloadTemplateImpl.class);

  private Client client = ClientBuilder.newClient();

  /**
   * (non-Javadoc)
   *
   * @see OpenloadTemplate#getAccountInfo(URI uri)
   */
  @Override
  public OpenloadResponse getAccountInfo(URI uri) {
    return client
        .target(uri)
        .request(MediaType.APPLICATION_JSON)
        .get(OpenloadResponse.class);
  }

  /**
   * (non-Javadoc)
   *
   * @see OpenloadTemplate#downloadTicket(URI uri)
   */
  @Override
  public OpenloadResponse downloadTicket(URI uri) {
    return client
        .target(uri)
        .request(MediaType.APPLICATION_JSON)
        .get(OpenloadResponse.class);
  }

  /**
   * (non-Javadoc)
   *
   * @see OpenloadTemplate#downloadLink()
   */
  @Override
  public void downloadLink() {

  }

  /**
   * (non-Javadoc)
   *
   * @see OpenloadTemplate#getFileInfo()
   */
  @Override
  public void getFileInfo() {

  }

  /**
   * (non-Javadoc)
   *
   * @see OpenloadTemplate#getUploadURL()
   */
  @Override
  public void getUploadURL() {

  }

  /**
   * (non-Javadoc)
   *
   * @see OpenloadTemplate#remoteUploadFile()
   */
  @Override
  public void remoteUploadFile() {

  }

  /**
   * (non-Javadoc)
   *
   * @see OpenloadTemplate#checkRemoteUploadStatus()
   */
  @Override
  public void checkRemoteUploadStatus() {

  }

  /**
   * (non-Javadoc)
   *
   * @see OpenloadTemplate#getFolderContent()
   */
  @Override
  public void getFolderContent() {

  }

  /**
   * (non-Javadoc)
   *
   * @see OpenloadTemplate#renameFolder()
   */
  @Override
  public void renameFolder() {

  }

  /**
   * (non-Javadoc)
   *
   * @see OpenloadTemplate#renameFile()
   */
  @Override
  public void renameFile() {

  }

  /**
   * (non-Javadoc)
   *
   * @see OpenloadTemplate#deleteFile()
   */
  @Override
  public void deleteFile() {

  }

  /**
   * (non-Javadoc)
   *
   * @see OpenloadTemplate#convertingFile()
   */
  @Override
  public void convertingFile() {

  }

  /**
   * (non-Javadoc)
   *
   * @see OpenloadTemplate#showRunningFileConverts()
   */
  @Override
  public void showRunningFileConverts() {

  }

  /**
   * (non-Javadoc)
   *
   * @see OpenloadTemplate#getSplashImage()
   */
  @Override
  public void getSplashImage() {

  }

}
