package net.kreddo.openload.template;

import java.net.URI;
import net.kreddo.openload.OpenloadResponse;

public interface OpenloadTemplate {

  /** Everything account related (total used storage, reward, ...) */
  OpenloadResponse getAccountInfo(URI uri);

  /** Preparing a Download */
  OpenloadResponse downloadTicket(URI uri);

  /** Get a download link by using download ticket */
  void downloadLink();

  /** Check the status of a file, e.g. if the file exists */
  void getFileInfo();

  /**
   * Get an Upload URL
   *
   * <p>Uploads shall be POSTed to the upload URL returned by our API and shall be
   * multipart/form-data encoded. Example with curl:
   *
   * <p>curl -F file1=@/path/to/file.txt https://13abc37.example.com/ul/jAZUhVzeU78
   */
  void getUploadURL();

  /** Remote Uploading a file */
  void remoteUploadFile();

  /** Check Status of Remote Upload */
  void checkRemoteUploadStatus();

  /** Shows the content of your folders */
  void getFolderContent();

  /** Set a new name for a folders */
  void renameFolder();

  /** Set a new name for a file */
  void renameFile();

  /** Remove one of your files */
  void deleteFile();

  /** Convert previously uploaded files to a browser-streamable format (mp4 / h.264) */
  void convertingFile();

  /** Shows running file converts by folder */
  void showRunningFileConverts();

  /** Shows the video splash image (thumbnail) */
  void getSplashImage();
}
