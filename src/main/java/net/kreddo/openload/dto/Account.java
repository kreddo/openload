package net.kreddo.openload.dto;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

public class Account {

  @JsonProperty("extid")
  private String existingUserId;
  @JsonProperty("email")
  private String email;
  @JsonProperty("signup_at")
  @JsonSerialize(as = Date.class)
  @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  private Date signUpDate;
  @JsonProperty("storage_left")
  private int storageLeft;
  @JsonProperty("storage_used")
  private int storageUsed;
  @JsonProperty("traffic")
  private Traffic traffic;
  @JsonProperty("balance")
  private double balance;

  public String getExistingUserId() {
    return existingUserId;
  }

  public void setExistingUserId(String existingUserId) {
    this.existingUserId = existingUserId;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Date getSignUpDate() {
    return signUpDate;
  }

  public void setSignUpDate(Date signUpDate) {
    this.signUpDate = signUpDate;
  }

  public int getStorageLeft() {
    return storageLeft;
  }

  public void setStorageLeft(int storageLeft) {
    this.storageLeft = storageLeft;
  }

  public int getStorageUsed() {
    return storageUsed;
  }

  public void setStorageUsed(int storageUsed) {
    this.storageUsed = storageUsed;
  }

  public Traffic getTraffic() {
    return traffic;
  }

  public void setTraffic(Traffic traffic) {
    this.traffic = traffic;
  }

  public double getBalance() {
    return balance;
  }

  public void setBalance(double balance) {
    this.balance = balance;
  }

  @Override
  public String toString() {
    return "Account{" +
        "existingUserId='" + existingUserId + '\'' +
        ", email='" + email + '\'' +
        ", signUpDate=" + signUpDate +
        ", storageLeft=" + storageLeft +
        ", storageUsed=" + storageUsed +
        ", traffic=" + traffic +
        ", balance=" + balance +
        '}';
  }
}
