package net.kreddo.openload.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Traffic {

  @JsonProperty("left")
  private int left;

  @JsonProperty("used_24h")
  private int usedLastDay;

  public int getLeft() {
    return left;
  }

  public void setLeft(int left) {
    this.left = left;
  }

  public int getUsedLastDay() {
    return usedLastDay;
  }

  public void setUsedLastDay(int usedLastDay) {
    this.usedLastDay = usedLastDay;
  }

  @Override
  public String toString() {
    return "Traffic{" +
        "left=" + left +
        ", usedLastDay=" + usedLastDay +
        '}';
  }
}
