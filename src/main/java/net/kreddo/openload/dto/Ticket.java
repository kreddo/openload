package net.kreddo.openload.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Ticket {

  @JsonProperty("ticket")
  private String ticketId;

  @JsonProperty("captcha_url")
  private String captchaUrl;

  @JsonProperty("captcha_w")
  private long captchaWidth;

  @JsonProperty("captcha_h")
  private long captchaHeight;

  @JsonProperty("wait_time")
  private long waitTime;

  @JsonProperty("valid_until")
  @JsonSerialize(as = Date.class)
  @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  private Date validUntil;

  public String getTicketId() {
    return ticketId;
  }

  public void setTicketId(String ticketId) {
    this.ticketId = ticketId;
  }

  public String getCaptchaUrl() {
    return captchaUrl;
  }

  public void setCaptchaUrl(String captchaUrl) {
    this.captchaUrl = captchaUrl;
  }

  public long getCaptchaWidth() {
    return captchaWidth;
  }

  public void setCaptchaWidth(long captchaWidth) {
    this.captchaWidth = captchaWidth;
  }

  public long getCaptchaHeight() {
    return captchaHeight;
  }

  public void setCaptchaHeight(long captchaHeight) {
    this.captchaHeight = captchaHeight;
  }

  public long getWaitTime() {
    return waitTime;
  }

  public void setWaitTime(long waitTime) {
    this.waitTime = waitTime;
  }

  public Date getValidUntil() {
    return validUntil;
  }

  public void setValidUntil(Date validUntil) {
    this.validUntil = validUntil;
  }

  @Override
  public String toString() {
    return "Ticket{" +
        "ticket='" + ticketId + '\'' +
        ", captchaUrl='" + captchaUrl + '\'' +
        ", captchaWidth=" + captchaWidth +
        ", captchaHeight=" + captchaHeight +
        ", waitTime=" + waitTime +
        ", validUntil=" + validUntil +
        '}';
  }
}
